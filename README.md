# Mastermind

This is server and client for the Mastermind challenge on IMEX 2020.

### How to run

Install dependencies with `npm install` (or whatever you like).

You'll need three environment variables: `PORT`, `EASY_FLAG` and `HARD_FLAG`.
Then just run `npm start`.

### Routes

`/challenge/easy` takes you to the game UI for the easy challenge.
`/easy` is the API route where you send your guess.

The same applies to the hard version of the challenge.
