var arrowOffset = 0
const PEGS = 4
const COLORS = 6
var currentGuess = Array(PEGS).fill(-1)

function generateCell(rowNumber, pegNumber) {
  return `<td>
    <div class="peg" onclick="clicked(${rowNumber}, ${pegNumber})"></div>
  </td>`
}

function generateResultsTable() {
  var resultsDiv = '<td><div class="results">'
  for (var resultPeg = 0; resultPeg < 4; resultPeg++) {
    resultsDiv += '<div class="result-peg"></div>'
  }
  resultsDiv += '</div></td>'
  return resultsDiv
}

function generateRow(rowNumber) {
  var row = '<tr>'
  for (var peg = 0; peg < 4; peg++) {
    row += generateCell(rowNumber, peg)
  }
  row += generateResultsTable()
  row += '</tr>'
  return row
}

function generateTable() {
  var table = ''
  for (var row = 0; row < TRIES; row++) {
    table += generateRow(row)
  }
  return table
}

var colors = ["#7ac74f", "#0892a5", "#fa8334", "#e9d758", "#f85a3e", "#995d81"]

function generateColors() {
  var colorsSelector = ''
  for (var color = 0; color < 6; color++) {
    colorsSelector +=
    `<div class="peg"
       id = "color${color}"
       style="background-color: ${colors[color]};"
       onclick="selectColor(${color})"
     ></div>`
  }
  return colorsSelector
}

function start() {
  currentRow = TRIES - 1

  const table = document.getElementById("game-table")
  table.innerHTML = generateTable()

  const colorsSelector = document.getElementById("colors")
  colorsSelector.innerHTML = generateColors()

  selectColor(0)

  const button = document.getElementById("send-button")
  button.disabled = true

  const arrow = document.getElementById("arrow")
  arrow.style.top = 0;
  arrowOffset = arrow.getBoundingClientRect().top;
  updateArrow()
}

function clicked(rowNumber, pegNumber) {
  if (currentRow !== rowNumber)
    return
  const table = document.getElementById("game-table");
  table.rows[rowNumber].cells[pegNumber].children[0].style.background = colors[selectedColor]

  updateGuess(pegNumber, selectedColor)
}

function updateGuess(pegNumber, color) {
  currentGuess[pegNumber] = color

  for (var peg = 0; peg < PEGS; peg++) {
    if (currentGuess[peg] < 0 || currentGuess[peg] >= COLORS)
      return
  }

  const button = document.getElementById("send-button")
  button.disabled = false
}

function sendRow() {
  const button = document.getElementById("send-button")
  button.disabled = true
  button.innerHTML = 'Send'

  prepareToSendRow()
  fetch(formURL())
    .then(resp => resp.json())
    .then(data => {
      loadResult(currentRow, data.result)

      currentGuess = Array(PEGS).fill(-1)
      currentRow--;
      if (data.flag === undefined && currentRow < 0) {
        alert("You lost, press OK to try again.")
        location.reload()
      }
      if (data.flag !== undefined) {
        alert(`You win, the flag is ${data.flag}`)
        location.reload()
      }

      button.innerHTML = 'Send'
      updateArrow()
    })
}

function updateArrow() {
  const table = document.getElementById("game-table")
  const currentRowRect = table.rows[currentRow].getBoundingClientRect()
  const mid = currentRowRect.top + currentRowRect.height / 2

  const arrow = document.getElementById("arrow")
  const arrowRect = arrow.getBoundingClientRect()
  arrow.style.top = mid - arrowRect.height / 2 - arrowOffset;
  arrow.style.left = currentRowRect.left - arrowRect.width - 10;
}

var selectedColor = 0

function selectColor(color) {
  const prevColor = document.getElementById(`color${selectedColor}`);
  const newColor = document.getElementById(`color${color}`);
  prevColor.style.borderColor = 'transparent'
  newColor.style.borderColor = 'black'
  
  selectedColor = color
}

function loadResult(row, result) {
  const table = document.getElementById("game-table");
  const resultPegs = table.rows[row].cells[PEGS].children[0]
  var currentPeg = 0
  for (var i = 0; i < result[0]; i++)
    resultPegs.children[currentPeg++].style.background = 'black'
  for (var i = 0; i < result[1]; i++)
    resultPegs.children[currentPeg++].style.background = 'white'
}

window.onresize = updateArrow
