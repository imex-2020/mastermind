const COLORS = 6
const PEGS = 4

function Mastermind() {
  if (arguments.callee.instance) {
    return arguments.callee.instance
  }

  arguments.callee.instance = this
  
  this.all = []
  function generateAll(instance, current, size) {
    if (size === PEGS) {
      instance.all.push(current.slice())
      return
    }
    for (var color = 0; color < COLORS; color++) {
      current.push(color)
      generateAll(instance, current, size + 1)
      current.pop()
    }
  }
  generateAll(this, [], 0)

  this.feedback = {}
  for (var password of this.all) {
    this.feedback[password] = {}
    for (var guess of this.all) {
      const result = [0, 0]

      // number of colored feedback pegs
      for (var peg = 0; peg < PEGS; peg++) {
        if (password[peg] === guess[peg]) {
          result[0]++;
        }
      }

      // total number of feedback pegs
      const color_frequency = Array(COLORS).fill(0)
      for (var peg = 0; peg < PEGS; peg++) {
        color_frequency[password[peg]]++
      }
      for (var peg = 0; peg < PEGS; peg++) {
        color_frequency[guess[peg]]--;
        if (color_frequency[guess[peg]] >= 0) {
          result[1]++;
        }
      }

      result[1] -= result[0];
      this.feedback[password][guess] = result
    }
  }

  this.isValid = (guess) => {
    if (guess.length !== PEGS) {
      return false
    }
    for (var peg = 0; peg < PEGS; peg++) {
      if (guess[peg] < 0 || guess[peg] >= COLORS) {
        return false
      }
    }
    return true
  }
}

module.exports = {
  PEGS,
  COLORS,
  Mastermind,
}
