const express = require('express')
const path = require('path')
const EasyChallenge = require('./easy')
const HardChallenge = require('./hard')

const app = express()

app.use('/', express.static(path.join(__dirname, '../client')))

app.get('/easy/api', function(req, res) {
  if (req.query.seed === undefined || req.query.guess == undefined) {
    res.status(400).send({
      error: 'query should have a seed and a valid guess',
    })
  } else {
    const seed = req.query.seed
    const guess = JSON.parse(req.query.guess)

    const result = EasyChallenge.getResult(seed, guess)
    if (result instanceof Error) {
      res.status(400).send({
        error: result.message
      })
    } else {
      const requestResult = {
        result,
      }
      if (result[0] === 4) {
        requestResult.flag = process.env.EASY_FLAG
      }
      res.status(200).send(requestResult)
    }
  }
})

app.get('/hard/api', function(req, res) {
  if (req.query.guesses == undefined) {
    res.status(400).send({
      error: 'query should have a list of guesses',
    })
  } else {
    const guesses = JSON.parse(req.query.guesses)

    const result = HardChallenge.getResult(guesses)
    if (result instanceof Error) {
      res.status(400).send({
        error: result.message
      })
    } else {
      const requestResult = {
        result,
      }
      if (result[0] === 4) {
        requestResult.flag = process.env.HARD_FLAG
      }
      res.status(200).send(requestResult)
    }
  }
})

console.log(`EASY_FLAG: ${process.env.EASY_FLAG}`)
console.log(`HARD_FLAG: ${process.env.HARD_FLAG}`)

const PORT = process.env.PORT
app.listen(PORT, () => {
  console.log('server running on port', PORT)
})
