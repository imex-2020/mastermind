const random = require('random')
const seedrandom = require('seedrandom')
const { COLORS, PEGS, Mastermind } = require('./mastermind')

function generatePassword(seed) {
  random.use(seedrandom(seed))
  const password = Array(PEGS)
  for (var peg = 0; peg < PEGS; peg++) {
    password[peg] = random.int(min = 0, max = COLORS - 1)
  }
  return password
}

function getResult(seed, guess) {
  const game = new Mastermind()

  if (game.isValid(guess) === false) {
    return new Error('invalid guess');
  }

  const password = generatePassword(seed)
  return game.feedback[password][guess]
}

module.exports = {
  getResult,
}
