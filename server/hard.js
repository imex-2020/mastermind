const { PEGS, COLORS, Mastermind } = require('./mastermind')

const MAX_TRIES = 5

function prunePossibilities(possibilities, guess) {
  const game = new Mastermind()

  const resultFrequency = {}
  for (var colored = 0; colored <= 4; colored++) {
    for (var white = 0; white + colored <= 4; white++) {
      resultFrequency[JSON.stringify([colored, white])] = []
    }
  }

  for (var possibility of possibilities) {
    const resultString = JSON.stringify(game.feedback[possibility][guess])
    resultFrequency[resultString].push(possibility)
  }

  var maxFrequency = 0
  var bestResult = JSON.stringify([0, 0])
  for (var result in resultFrequency) {
    if (resultFrequency[result].length > maxFrequency) {
      bestResult = result
      maxFrequency = resultFrequency[result].length
    }
  }

  return [JSON.parse(bestResult), resultFrequency[bestResult]]
}

function getResult(guesses) {
  if (guesses.length > MAX_TRIES) {
    return new Error('too many guesses')
  }

  const game = new Mastermind()

  var bestResult
  var possibilities = game.all.slice()
  for (var guess of guesses) {
    if (game.isValid(guess) === false) {
      return new Error('invalid guess')
    }
    const [result, newPossibilities] = prunePossibilities(possibilities, guess)
    possibilities = newPossibilities
    bestResult = result
  }
  return bestResult
}

module.exports = {
  getResult,
}
